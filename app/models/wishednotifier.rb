require 'sendwithus_ruby_action_mailer'
class Wishednotifier < ::SendWithUsMailer::Base

  default from: 'atendimento@dragonflyvc.com'

  def order_wished(order, email)
      assign :user, {email: email}

      assign :product, {id: order}

      mail(
          email_id: 'tem_bF4SMRBMPv2mjyV4SgZ6p3',
          recipient_address: "marcelo@versuti.com.br",
      )
  end
end
